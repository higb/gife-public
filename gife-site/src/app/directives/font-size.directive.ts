import { Directive, Input, OnInit, ElementRef, HostBinding, AfterViewInit, ViewChild, ViewContainerRef, ComponentRef } from '@angular/core';

@Directive({
  selector: '[appChangeFontSize]'
})
export class FontSizeDirective implements OnInit, AfterViewInit {
  @Input() setSize: number = 1;
  @HostBinding('style.fontSize') fontSize: string;
  
  
  constructor(
    private elementRef: ElementRef,
    private viewCR: ViewContainerRef
  ) {}

  ngOnInit() {
    // this.elementRef.nativeElement.fontSize = this.elementRef.nativeElement.fontSize + '1'
    console.log('font-size', this.fontSize);
    // this.fontSize = `${this.setSize}rem`;

    const currentFontSize = getComputedStyle(this.elementRef.nativeElement).getPropertyValue('font-size');

    console.log('currentFontSize', currentFontSize);

    if (currentFontSize) {
      const pxToRem = +(currentFontSize.replace('px', '')) / 16 ;
      console.log('pxToRem', pxToRem);
      this.fontSize = `clamp(3rem, 3.5vw, ${this.setSize + pxToRem}rem)`;
    } else {
      this.fontSize = `clamp(3rem, 3.5vw, ${this.setSize}rem)`;
    }
  }

  ngAfterViewInit() {
    console.log('viewCR', this.viewCR.element);
  }
}
