import { subscribe } from 'graphql';
import { Directive, Input, OnInit, OnDestroy } from '@angular/core';
declare const frappe: any;
import { Globals } from '../globals';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appFrappe]'
})
export class FrappeDirective implements OnInit, OnDestroy {
  @Input() data: {};
  @Input() title: string;
  @Input() type: string;
  @Input() height: number;

  currentBreakpoint: string = '';
  bpObserverSubscription: Subscription;

  constructor(
    private globals: Globals,
    private bpObserver: BreakpointObserver,
  ) { }

  ngOnInit() :void {
    const bpArray = ['(min-width: 42.5rem)', '(min-width: 67.5rem)', '(min-width: 80rem)'];

    const frappeChartInstance = new frappe.Chart("#chart", {  // or a DOM element,
      // new Chart() in case of ES6 module with above usage
      title: this.title || null,
      data: this.data,
      type: this.type || 'bar', // or 'bar', 'line', 'scatter', 'pie', 'percentage'
      height: this.height || 250,
      colors: [this.globals.gifeColor],
      valuesOverPoints: true,
      tooltipOptions: {
        // formatTooltipX: null,
        formatTooltipY: (d: number) => new Intl.NumberFormat('it-IT', { style: 'currency', currency: 'EUR', maximumSignificantDigits: 10 }).format(Math.abs(Number(d) * 1.0e+6)),
      },
    })
    console.log(frappeChartInstance);
    this.bpObserverSubscription = this.bpObserver.observe(bpArray)
        .subscribe(result => {
          switch (true) {
            case result.breakpoints[bpArray[0]] && !result.breakpoints[bpArray[1]] || result.breakpoints[bpArray[1]] && !result.breakpoints[bpArray[2]]:
              this.currentBreakpoint = 'tablet';
              break;
            case result.breakpoints[bpArray[2]]:
              // desktop
              this.currentBreakpoint = 'desktop';
              break;
            default:
              // mobile 0 -> 42.5rem
              this.currentBreakpoint = 'mobile';
          }
      });
  }

  ngOnDestroy(): void {
    this.bpObserverSubscription.unsubscribe();
  }

}
