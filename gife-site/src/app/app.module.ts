import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router, Scroll, Event } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData, ViewportScroller } from '@angular/common';

import { InViewportModule } from 'ng-in-viewport';

import { filter } from 'rxjs/operators';
import { CookieModule } from 'ngx-cookie';


import { LOCALE_ID } from '@angular/core';
import localeIt from '@angular/common/locales/it';

import { AppRoutingModule } from './app-routing.module';
import { GoogleMapModule } from './components/goo-map/goo-map.module';

import { AppComponent } from './app.component';

import { Globals } from './globals';
// import * as globalTypes from './interfaces/types';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  SprkListItemModule,
  SprkMastheadModule,
  SprkModalModule,
  SprkStackModule,
  SprkUnorderedListModule,
  SprkBoxModule,
  SprkTextModule,
  SprkHeadingModule,
  SprkLinkDirectiveModule,
  SprkStackItemModule
} from "@sparkdesignsystem/spark-angular";


import { setContext } from 'apollo-link-context';
import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import introspectionQueryResultData from '../fragmentTypes.json';
import { GraphQLModule } from './graphql.module';
import { LayoutModule } from '@angular/cdk/layout';

// DIRECTIVES
import { FontSizeDirective } from './directives/font-size.directive';

// RESOLVERS
import { WorksResolver } from './works-resolver.service';

// EXTERNAL COMPONENTS
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

// PAGE COMPONENTS
import { HomepageComponent } from './pages/homepage/homepage.component';

// COMPONENTS
import { NewsComponent } from './components/news/news.component';
import { InfoDataComponent } from './components/info-data/info-data.component';
import { IconStripComponent } from './components/icon-strip/icon-strip.component';
import { PortfolioSwiperComponent } from './components/portfolio-swiper/portfolio-swiper.component';
import { ClientsSwiperComponent } from './components/clients-swiper/clients-swiper.component';
import { AwardDownloaderComponent } from './components/award-downloader/award-downloader.component';
import { ActionCallerComponent } from './components/action-caller/action-caller.component';
import { FooterComponent } from './components/footer/footer.component';



registerLocaleData(localeIt, 'it-IT');

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
});

const token = '';

const autLink = setContext((_, { headers }) => {
  return {
    headers: Object.assign(
      headers || {},
      {
        'content-type': 'application/json',
        'accept': 'application/json',
        'authorization': `Bearer ${token}`
      }
    )
  }
});

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

@NgModule({
  declarations: [
    AppComponent,
    FontSizeDirective,
    NewsComponent,
    InfoDataComponent,
    IconStripComponent,
    PortfolioSwiperComponent,
    ClientsSwiperComponent,
    AwardDownloaderComponent,
    ActionCallerComponent,
    FooterComponent,
    HomepageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FontAwesomeModule,

    SprkModalModule,
    SprkMastheadModule,
    SprkStackModule,
    SprkStackItemModule,
    SprkListItemModule,
    SprkUnorderedListModule,
    SprkBoxModule,
    SprkTextModule,
    SprkHeadingModule,
    SprkLinkDirectiveModule,

    SwiperModule,

    GraphQLModule,
    LayoutModule,
    GoogleMapModule,
    CookieModule.forChild(),
    InViewportModule,
  ],
  providers: [
    Globals,
    WorksResolver,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
    { provide: LOCALE_ID, useValue: 'it-IT' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    router: Router,
    viewportScroller: ViewportScroller,
  ) {
    router.events
    .pipe(
      filter((e: Event): e is Scroll => e instanceof Scroll)
    )
    .subscribe(e => {
      console.warn('event', e)
      if (e['position']) {
        // backward navigation
        viewportScroller.scrollToPosition(e['position']);
        console.warn('SCROLL POSITION')
      } else if (e['anchor']) {
        // anchor navigation
        viewportScroller.scrollToAnchor(e['anchor']);
        console.warn('SCROLL ANCHOR')
      } else {
        // forward navigation
        console.warn('FORWARD NAVIGATION');
      }
    })
  }
}
