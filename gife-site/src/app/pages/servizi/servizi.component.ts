import { ViewportScroller } from '@angular/common';
import { SplitView } from './../../interfaces/types';
import { ReplaceStringPipe } from './../../replace-string.pipe';
import { SplitViewComponent } from './../../components/split-view/split-view.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, Input, AfterViewInit, ViewChildren, ElementRef, QueryList, ViewContainerRef } from '@angular/core';
import { GlobalUIService } from 'src/app/global-ui.service';
import { ServicesGQLService } from 'src/app/services/services-gql.service';

interface Services {
  serviceTitle: string,
  serviceContent: string,
  serviceImage: {
    url: ''
  },
  serviceBlock: Object[]
}


@Component({
  selector: 'app-servizi',
  templateUrl: './servizi.component.html',
  styleUrls: ['./servizi.component.scss']
})
export class ServiziComponent implements OnInit, AfterViewInit {
  service_content: Services;
  currentFragment: string;

  @ViewChildren(SplitViewComponent) prova: QueryList<any>
  // @ViewChildren(SplitViewComponent, { read: ElementRef }) prova: QueryList<any>
  // @ViewChildren(SplitViewComponent, { read: ViewContainerRef }) prova: QueryList<ViewContainerRef>

  constructor(
    private globalUIService: GlobalUIService,
    private serviziGqlService: ServicesGQLService,
    private route: ActivatedRoute,
    private router: Router,
    private viewportScroller: ViewportScroller,
  ) { }

  ngOnInit() {
    this.serviziGqlService.watch()
    .valueChanges
    .subscribe((response: any) => {
      console.log('servizi query', response);
      this.service_content = response.data.servizi;
    });
  }

  ngAfterViewInit() {
    if (!this.route.snapshot.fragment) {
      this.viewportScroller.scrollToPosition([0, 0]);
      console.warn('SCROLL 0 0')
    }
  }

}
