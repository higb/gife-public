import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../components/page-header/page-header.module';
import {
  SprkHeadingModule,
  SprkStackItemModule,
  SprkStackModule,
  SprkTextModule
} from "@sparkdesignsystem/spark-angular";
import { ServiziRoutingModule } from './servizi-routing.module';

import { ServiziComponent } from './servizi.component';
import { SplitViewComponent } from '../../components/split-view/split-view.component';

import { ReplaceStringPipe } from '../../replace-string.pipe';



@NgModule({
  declarations: [
    ServiziComponent,
    SplitViewComponent,
    ReplaceStringPipe,
  ],
  imports: [
    CommonModule,
    ServiziRoutingModule,
    PageHeaderModule,

    SprkStackModule,
    SprkStackItemModule,
    SprkHeadingModule,
    SprkTextModule,
  ]
})
export class ServiziModule { }
