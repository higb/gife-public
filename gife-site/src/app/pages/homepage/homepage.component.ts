import { ViewportScroller } from '@angular/common';
import { ApolloQueryResult } from 'apollo-client';
import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, ViewChildren, QueryList, HostListener } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { debounceTime, switchMap, throttleTime } from 'rxjs/operators';
import { HomedataService } from '../../services/homedata.service';
import { CommondataService } from '../../commondata.service';

import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import { DomSanitizer } from '@angular/platform-browser';

import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { GlobalUIService } from 'src/app/global-ui.service';

const heroSwiperConfig: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 1,
  loop: true,
  autoplay: {
    delay: 5000,
  },
}

const portfolioSwiperConfig: SwiperConfigInterface = {
  // direction: 'horizontal',
  slidesPerView: 1.1,
  //slidesOffsetBefore: 300,
  loop: true,
  loopAdditionalSlides: 2,
  loopedSlides: 3,
  centeredSlides: true,
  spaceBetween: 5,
  freeMode: true,
  initialSlide: 2,
  breakpoints: {
    680: {
      slidesPerView: 2.2,
    }
  },
  autoplay: {
    delay: 5000,
  },
};

interface Homepage {
  home: {
    homeCarousel: {},
    azienda: {},
    clienti: {},
    certificazioni: {},
  },
  servizi: {}
  allNews: {},
  allWorks: {},
}

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit, AfterViewInit {
  homepage_content: Homepage;
  brand_icon = '';

  portfolioParams: {} = {
    right: true,
    top: true,
    switchColor: true
  }

  clientsSizes = {
    a: 120,
    b: 60,
    c: 30
  }

  heroSwiperConfig = heroSwiperConfig;
  portfolioSwiperConfig = portfolioSwiperConfig;
  currentSize = new Subject;

  @HostListener('window:resize', ['$event.target'])
  public onResize(target) {
    this.currentSize.next(target.innerWidth);
  }

  @ViewChildren('homeSlider', { read: ElementRef }) homeSlider:QueryList<ElementRef>;

  constructor(
    private route: ActivatedRoute,
    private homedataService: HomedataService,
    public commondataService: CommondataService,
    public globalUIService: GlobalUIService,
    private viewportScroller: ViewportScroller,
  ) {
    this.currentSize
    .asObservable()
    .pipe(
      debounceTime(500)
    )
    .subscribe(innerWidth => {
      console.log('innerWidth:', innerWidth)
      let vh = window.innerHeight * 0.01;
      console.log('EV', this.currentSize);
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    });
  }

  ngOnInit() {

    this.homedataService.watch()
    .valueChanges
    .subscribe((result: ApolloQueryResult<Homepage>) => {
      console.log('myquery', result);
      this.homepage_content = result.data;
    });
  }

  ngAfterViewInit() {
    this.viewportScroller.scrollToPosition([0, 0]);
    console.warn('SCROLL 0 0')

    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  }

}
