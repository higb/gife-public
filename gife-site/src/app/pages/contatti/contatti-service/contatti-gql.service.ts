import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import GraphQL from '../../../services/fragments';


const gifeContattiQuery = gql`
query {
  contact {
    content
    image {
      responsiveImage(imgixParams: {auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
        ...responsiveImageFragment
      }
      url
    }
    title
    contatti {
      descrizioneContatto
      iconaContatto
      titoloContatto
      coordinate {
        latitude
        longitude
      }
    }
  }
}
${GraphQL.fragments.images}
`;

@Injectable({
  providedIn: 'root'
})
export class ContattiGqlService extends Query {
  document = gifeContattiQuery;
}
