import { TestBed } from '@angular/core/testing';

import { ContattiGqlService } from './contatti-gql.service';

describe('ContattiGqlService', () => {
  let service: ContattiGqlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ContattiGqlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
