import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../components/page-header/page-header.module';
import { TextModule } from './../../components/text/text.module';
import { GoogleMapModule } from './../../components/goo-map/goo-map.module';
import { ContattiRoutingModule } from './contatti-routing.module';

import { ContattiComponent } from './contatti.component';



@NgModule({
  declarations: [
    ContattiComponent,
  ],
  imports: [
    CommonModule,
    ContattiRoutingModule,
    PageHeaderModule,
    TextModule,
    GoogleMapModule
  ]
})
export class ContattiModule { }
