import { ViewportScroller } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from 'apollo-client';
import { ContattiGqlService } from './contatti-service/contatti-gql.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';

interface IContact {
  contact: {
    title: string;
    content: string;
    image: {
      url: string;
    }
    contatti: Array<{
      descrizioneContatto: string;
      iconaContatto: string;
      titoloContatto: string;
      coordinate: {
        latitude: number;
        longitude: number;
      }
    }>
  }
}

@Component({
  selector: 'app-contatti',
  templateUrl: './contatti.component.html',
  styleUrls: ['./contatti.component.scss']
})
export class ContattiComponent implements OnInit, AfterViewInit {
  contattiContent: IContact;
  contactsText: object[];

  constructor(
    private contattiGqlService: ContattiGqlService,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller,
  ) {}

  ngOnInit(): void {
    this.contattiGqlService.watch()
    .valueChanges
    .subscribe((result: ApolloQueryResult<IContact>) => {
      this.contattiContent = result.data;
      this.contactsText = this.contattiContent.contact.contatti;
    })
  }

  ngAfterViewInit(): void {
    if (!this.route.snapshot.fragment) {
      this.viewportScroller.scrollToPosition([0, 0]);
      console.warn('SCROLL 0 0')
    }
  }

}
