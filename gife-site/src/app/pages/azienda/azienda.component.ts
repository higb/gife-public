import { ViewportScroller } from '@angular/common';
import { ApolloQueryResult } from 'apollo-client';
import { subscribe } from 'graphql';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AziendaGQLService } from './azienda-service/azienda-gql.service';

interface IAzienda {
  aboutPage: {
    content: string;
    title: string;
    allineaAlCentro: boolean;
    bigQuote: boolean;
    autoreCitazione: string;
    image: {
      url: string;
    }
    brochure: {
      url: string;
    }
    heading: string;
    paragraph: string;
    keyConcept: string;
    chart: Array<{
      anno: string;
      fatturato: number;
    }>;
    chartTitle: string;
    chartDescription: string;
    diagrammaAziendale: {
      responsiveImage: {
        sizes: string;
        src: string;
        srcSet: string;
        webpSrcSet: string;
        alt: string;
      };
      url: string;
    }
    strutturaOrganizzativaTitle: string;
  }
}

interface IFrappeData {
  labels: string[];
  datasets: Array<{
    name?: string;
    type?: string;
    values: number[];
  }>;
}

@Component({
  selector: 'app-azienda',
  templateUrl: './azienda.component.html',
  styleUrls: ['./azienda.component.scss']
})
export class AziendaComponent implements OnInit, AfterViewInit {
  aziendaContent: IAzienda;
  keyConceptElements: string[];
  frappedata: IFrappeData = {
    labels: [],
    datasets: [
      {
        values: []
      }
    ]
  };

  constructor(
    private aziendaGQLService: AziendaGQLService,
    private viewportScroller: ViewportScroller,
  ) { }

  ngOnInit(): void {
    this.aziendaGQLService.watch()
    .valueChanges
    .subscribe((result: ApolloQueryResult<IAzienda>) => {
      this.aziendaContent = result.data;
      this.aziendaContent.aboutPage.chart.map((column: any)=> {
        this.frappedata.labels.push(column.anno);
        this.frappedata.datasets[0].values.push(column.fatturato);
      });


      this.keyConceptElements = result.data.aboutPage.keyConcept.split(';\n');
    })
  }

  ngAfterViewInit(): void {
    this.viewportScroller.scrollToPosition([0, 0]);
    console.warn('SCROLL 0 0')
  }

}
