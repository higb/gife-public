import { TestBed } from '@angular/core/testing';

import { AziendaGqlService } from './azienda-gql.service';

describe('AziendaGqlService', () => {
  let service: AziendaGqlService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AziendaGqlService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
