import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import GraphQL from '../../../services/fragments';


const gifeAziendaQuery = gql`
query {
  aboutPage {
    content
    title
    allineaAlCentro
    bigQuote
    autoreCitazione
    image {
      responsiveImage(imgixParams: {fit: crop, w: "1920", h: "1280", auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
        ...responsiveImageFragment
      }
      url
    }
    brochure {
      url
    }
    paragraph
    heading
    keyConcept
    chart {
      anno
      fatturato
    }
    chartTitle
    chartDescription
    diagrammaAziendale {
      responsiveImage(imgixParams: {auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
        ...responsiveImageFragment
      }
      url
    }
    strutturaOrganizzativaTitle
  }
}
${GraphQL.fragments.images}
`;

@Injectable({
  providedIn: 'root'
})
export class AziendaGQLService extends Query {
  document = gifeAziendaQuery;
}
