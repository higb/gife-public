import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../components/page-header/page-header.module';
import {
  SprkStackItemModule,
  SprkStackModule,
  SprkHeadingModule,
  SprkTextModule
} from "@sparkdesignsystem/spark-angular";
import { TextModule } from './../../components/text/text.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AziendaRoutingModule } from './azienda-routing.module';

import { AziendaComponent } from './azienda.component';
import { ChartComponent } from '../../components/chart/chart.component';
import { BulletPostsComponent } from '../../components/bullet-posts/bullet-posts.component';

import { FrappeDirective } from '../../directives/frappe.directive';



@NgModule({
  declarations: [
    AziendaComponent,
    ChartComponent,
    BulletPostsComponent,
    FrappeDirective,
  ],
  imports: [
    CommonModule,
    AziendaRoutingModule,
    PageHeaderModule,
    TextModule,
    FontAwesomeModule,

    SprkStackModule,
    SprkStackItemModule,
    SprkHeadingModule,
    SprkTextModule,
  ]
})
export class AziendaModule { }
