import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageHeaderModule } from './../../components/page-header/page-header.module';
import {
  SprkHeadingModule,
  SprkIconModule,
  SprkStackItemModule,
  SprkStackModule,
  SprkTextModule,
} from "@sparkdesignsystem/spark-angular";
import { SwiperModule } from 'ngx-swiper-wrapper';
import { WorksRoutingModule } from './works-routing.module';

import { WorksComponent } from './works.component';
import { ProjectDetailComponent } from '../../components/project-detail/project-detail.component';



@NgModule({
  declarations: [
    WorksComponent,
    ProjectDetailComponent,
  ],
  imports: [
    CommonModule,
    WorksRoutingModule,
    PageHeaderModule,
    SwiperModule,

    SprkHeadingModule,
    SprkTextModule,
    SprkStackModule,
    SprkStackItemModule,
    SprkIconModule,
  ]
})
export class WorksModule { }
