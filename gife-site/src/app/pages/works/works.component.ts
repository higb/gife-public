import { ViewportScroller } from '@angular/common';
import { subscribe } from 'graphql';
import { CommondataService } from './../../commondata.service';
import { WorksResult, Project } from './../../interfaces/types';
import { ApolloQueryResult } from 'apollo-client';
import { Apollo } from 'apollo-angular';
import { Component, OnDestroy, OnInit, ViewChildren, ElementRef, QueryList, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router, Data } from '@angular/router';
import { WorksService } from '../../services/works.service';
import { WorksGQLService } from '../../services/works-gql.service';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';

const allFilter = { filterName: 'All' };

@Component({
  selector: 'app-works',
  templateUrl: './works.component.html',
  styleUrls: ['./works.component.scss']
})
export class WorksComponent implements OnInit, AfterViewInit, OnDestroy {
  pageInfo: any; // TODO: type
  allProjects: any = []; // TODO: type
  projectsFilter: any = [] // TODO: type
  filteredProjects: any = []; // TODO: type
  currentProjectID: string = '';
  selectedProject = {};
  projectListTitle: string = 'Progetti';
  projectSelected: string;
  projectSubscription: Subscription;
  filterSubscription: Subscription;
  pageHeaderSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public worksService: WorksService,
    private worksGqlService: WorksGQLService,
    private commondataService: CommondataService,
    private viewportScroller: ViewportScroller,
  ) {
    this.projectSubscription = this.worksService.changeProjectEmitter.subscribe(
      ({idProject}) => {
        this.selectedProject = this.allProjects.filter((prj:Project) => prj.id === idProject)[0];
        this.currentProjectID = idProject;
        this.worksService.isProjectOpen = true;
        this.worksService.projectDetailSwiperIndex = 0;
        this.projectSelected = this.worksService.isProjectOpen ? `Altri progetti - ${this.worksService.currentFilter}` : `${this.worksService.currentFilter}`;
        this.projectListTitle = (!this.worksService.currentFilter || this.worksService.currentFilter === 'Tutte le opere') ? 'Progetti' : this.projectSelected;
      }
    );

    this.filterSubscription = this.worksService.changeFilterEmitter.subscribe(
      (filter: string) => {
        this.worksService.currentFilter = filter;
        this.worksService.dropdownChoices.map((choice) => {
          choice['active'] = choice['value'] === filter ? true : false
        })
        this.worksService.isProjectOpen = false;
        this.projectSelected = this.worksService.isProjectOpen ? `Altri progetti - ${this.worksService.currentFilter}` : `${this.worksService.currentFilter}`;
        this.projectListTitle = (!this.worksService.currentFilter || this.worksService.currentFilter === 'Tutte le opere') ? 'Progetti' : this.projectSelected;
      }
    );
  }

  ngOnInit(): void {
    console.log('****TEST', this.worksService.getProjects());
      this.worksGqlService.watch()
        .valueChanges.subscribe((result: ApolloQueryResult<WorksResult>) => {
          if (!this.allProjects.length) {
            this.pageInfo = result.data.operePage;
            this.allProjects = result.data.allWorks;
            this.filteredProjects = this.allProjects;
            this.projectsFilter = result.data.allWorksFilters;
            this.currentProjectID = this.route.snapshot.params['idProject'];

            this.allProjects.forEach((prj) => {
              prj.prjFilters = [];
              prj.categoria.forEach((filter) => prj.prjFilters.push(filter.filterName));
            });

            if (this.currentProjectID) {
              this.selectedProject = this.allProjects.filter((prj:Project) => prj.id === this.currentProjectID)[0];
            }

            this.worksService.isProjectOpen = this.route.snapshot.data['openProject'];
          }
        });

        this.projectSelected = this.worksService.isProjectOpen ? `Altri progetti - ${this.worksService.currentFilter}` : `${this.worksService.currentFilter}`;
        this.projectListTitle = (!this.worksService.currentFilter || this.worksService.currentFilter === 'Tutte le opere') ? 'Progetti' : this.projectSelected;
      }

    ngAfterViewInit(): void {
      if (this.route.snapshot.data['openProject']) {
        this.pageHeaderSubscription = this.commondataService.pageHeaderEmitter.subscribe(
          (pageHeaderElement: ElementRef) => {
            if (pageHeaderElement.nativeElement.offsetHeight > 0) {
              setTimeout(() => {
                this.worksService.scrollToProject();
              }, 500)
            }
          }
        )
      } else {
        this.viewportScroller.scrollToPosition([0, 0]);
        console.warn('SCROLL 0 0')
      }
    }

    ngOnDestroy() {
      this.projectSubscription.unsubscribe();
      this.filterSubscription.unsubscribe();

      if (this.route.snapshot.data['openProject']) {
        this.pageHeaderSubscription.unsubscribe();
      }

      if (!this.router.routerState.snapshot.url.includes('opere')) {
        this.worksService.currentFilter = 'Tutte le opere';
        this.worksService.isProjectOpen = false;
      }
    }


}
