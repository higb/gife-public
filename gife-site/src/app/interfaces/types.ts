import { Injectable } from '@angular/core';

@Injectable()
export class Header {
    data: {
        header: {
            brandEventDescription: string;
        }
    }
}

export interface Project {
    id: string,
    title: string,
    location: string,
    description: string,
    stato: string,
    valore: string,
    riferimento: string,
    committente: string,
    categoria: [{
        filterName: string
    }],
    coverImage: {
        responsiveImage: {
            sizes: string,
            src: string,
            srcSet: string,
            webpSrcSet: string,
            alt: string,
        },
        url: string,
    },
    gallery: [
        {
            responsiveImage: {
                sizes: string,
                src: string,
                srcSet: string,
                webpSrcSet: string,
                alt: string,
            },
            url: string,
            width: number,
            height: number,
        }
    ]
}

export interface PageInfo {
    title: 'string',
    content: 'string',
    image: {
        url: 'string'
    }
}

export interface ProjectFilter {
    filterName: string
}

interface Projects {
    projects: Project[]
}
export interface WorksResult {
    operePage: PageInfo,
    allWorks: Project[],
    allWorksFilters: ProjectFilter[]
}

export type SplitView = {
    content: {
        serviceName: string;
    }
}

export interface ModalData {
    flag: boolean;
    title: string;
    content?: string | HTMLElement;
    fragment?: boolean;
    type?: string;
}
