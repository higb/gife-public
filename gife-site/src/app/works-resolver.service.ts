import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';

import { WorksService } from './services/works.service';
import { subscribe } from 'graphql';
import { ApolloQueryResult } from 'apollo-client';
import { WorksGQLService } from './services/works-gql.service';
import { WorksResult } from './interfaces/types';

interface Project {
  id: number,
  name: string
}

interface Projects {
  projects: Project[]
}

@Injectable()
export class WorksResolver implements Resolve<Projects | Subscription>
{

  constructor(
    private worksService: WorksService,
    private worksGqlService: WorksGQLService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Projects> | Promise<Projects> | Projects | Subscription {
    return this.worksGqlService.watch()
      .valueChanges
      .subscribe( (result: ApolloQueryResult<WorksResult>) => {
        this.worksService.pageInfo = result.data['operePage'];
        this.worksService.allProjects = result.data['allWorks'];
        this.worksService.projectsFilter = result.data['allWorksFilters'];
        console.warn('RESOLVER this.allProjects', this.worksService.allProjects)
      });
  }
}
