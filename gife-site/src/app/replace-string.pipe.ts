import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'replaceString'
})
export class ReplaceStringPipe implements PipeTransform {

  transform(value: string) {
    return value.replace(new RegExp(' ', 'g'), '_');
  }

}
