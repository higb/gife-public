import { NgModule } from '@angular/core';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLink } from 'apollo-angular/http';
import { InMemoryCache } from 'apollo-cache-inmemory';

import { IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import introspectionQueryResultData from '../fragmentTypes.json';
import { setContext } from 'apollo-link-context';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
});

const autLink = setContext((_, { headers }) => {
  return {
    headers: Object.assign(
      headers || {},
      {
        'content-type': 'application/json',
        'accept': 'application/json',
        'authorization': `Bearer ${token}`
      }
    )
  }
});

export function createApollo(httpLink) {
  return {
    // link: httpLink.create({uri}),
    link: autLink.concat(httpLink.create({
      uri: "https://graphql.datocms.com/"
    })),
    cache: new InMemoryCache({ fragmentMatcher }),
  };
}

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink],
    },
  ],
})
export class GraphQLModule {}
