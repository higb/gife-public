import { Injectable } from '@angular/core';

@Injectable()
export class Globals {
  urlDomain = 'http://localhost:8888';
  urlPath = '/gife';
  gifeColor: string = '#EA3423';
}
