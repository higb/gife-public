import { GlobalUIService } from './global-ui.service';
import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';
import { CookieOptions, CookieService } from 'ngx-cookie';
import { ModalData } from './interfaces/types';

@Injectable({
  providedIn: 'root'
})
export class CommondataService {
  modalVisible: boolean = false;
  modalVisibilityUpdate = new EventEmitter<ModalData>();
  sendElementRef = new EventEmitter<ElementRef>();
  pageHeaderEmitter = new EventEmitter<ElementRef>();
  mastheadTopDistance: number;
  mastheadHeight: number;
  cookieName: string = 'GIFE-CookieConsent';
  cookieValue: string = 'accepted';
  cookieOpt: CookieOptions = {
    path: '/',
    expires: '',
    sameSite: 'lax',
  };
  loadedAPI: boolean = false;
  addresses: object[] = [];
  mapCenter = undefined;
  markerPositions: google.maps.LatLngLiteral[] = [];
  isSocial: boolean = false;

  constructor(
    private router: Router,
    private cookieService: CookieService,
    private globalUIService: GlobalUIService,
    ) {}

  setModal(flag: boolean) {
    this.modalVisible = flag;
  }

  scroll(el) {
    window.scrollTo(0, el);
  }

  openModal(flag: boolean, title: string, content?: string | HTMLElement, type: string = 'info') {
    console.log(window.pageYOffset);
    console.log('service', flag, title, content);
    const modalData = {flag, title, content, type}
    // this.commondataService.setModal(flag);
    this.modalVisibilityUpdate.emit(modalData);
  }

  setCookie(key: string, value: string, options?: CookieOptions): void {
    this.cookieService.put(key, value, options);
  }

  getCookie(key: string): string{
    return this.cookieService.get(key);
  }

  modalSetActions(modalType: string, cookieValue?: string) {

    const days = cookieValue === 'accepted' ? 365 : 15
    this.setExpires(days);

    if (modalType === 'choice') {
      this.setCookie(this.cookieName, cookieValue, this.cookieOpt);
      this.setModal(false);
    } else {
      this.setModal(false);
    }
  }

  setExpires(days = 1) {
    const date = new Date()
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);

    this.cookieOpt.expires = date;
  }

  setMapCenter(latitude: number, longitude: number) {
    this.mapCenter = {lat: latitude, lng: longitude + 0.0004000}
  }

}
