import {Injectable} from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';
import GraphQL from './fragments';


const gifeHomeQuery = gql`
query {
    home {
      homeCarousel {
        slideText
        slideHeading
        slideImage {
          responsiveImage(imgixParams: {fit: crop, w: "1920", h: "1400", auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
            ...responsiveImageFragment
          }
          url
          width
          height
        }
      }
      azienda {
        ... on DisplayinfoRecord {
          title
          content
        }
        ... on QualityRecord {
          qualityDescription
          qualityTitle
        }
      }
      clienti {
        partnersTitle
        partnersList {
          url
          width
          height
        }
      }
      certificazioni {
        certificateName
        certificateList
        certificateFile {
          url
        }
      }
      jobTitle
      jobMail
      jobDescription
      jobImage {
        responsiveImage(imgixParams: {fit: crop, w: "1920", h: "1280", auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
          ...responsiveImageFragment
        }
        url
      }
    }
    allWorks {
      id
      coverImage {
        responsiveImage(imgixParams: {fit: crop, maxW: "680", maxH: "400", auto: compress}) {
          ...responsiveImageFragment
        }
        url
      }
      title
    }
    allNews(orderBy: _publishedAt_DESC, filter: {_status: {eq: published}}) {
      _publishedAt
      newsTitle
      newsDetail
      newsCalendar
      newsAbstract
    }
    servizi {
      serviceBlock {
        mainIcon {
          url
        }
        serviceName
        serviceContent
      }
      serviceContent
      serviceTitle
      serviceImage {
        url
      }
    }
  }
  ${GraphQL.fragments.images}
`;

@Injectable({
  providedIn: 'root'
})
export class HomedataService extends Query {
  document = gifeHomeQuery;
}
