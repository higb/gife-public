import { Injectable } from '@angular/core';
import { Query } from 'apollo-angular';
import gql from 'graphql-tag';
import GraphQL from './fragments';


const gifeServicesQuery = gql`
query {
  servizi {
    serviceBlock {
      mainIcon {
        url
      }
      serviceName
      serviceContent
      singleServiceImage {
        responsiveImage(imgixParams: {auto: compress, w:"620", h:"620"}, sizes: "(max-width: 465px) 465px, 620px") {
          ...responsiveImageFragment
        }
        url
      }
      invertImagePosition
    }
    serviceContent
    serviceTitle
    serviceImage {
      responsiveImage(imgixParams: {auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
        ...responsiveImageFragment
      }
      url
    }
  }
}
${GraphQL.fragments.images}
`;

@Injectable({
  providedIn: 'root'
})
export class ServicesGQLService extends Query {
  document = gifeServicesQuery;
}
