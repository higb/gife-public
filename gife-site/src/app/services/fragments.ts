import gql from 'graphql-tag';

const GraphQL = {
  fragments: {
    images: {}
  }
}

GraphQL.fragments = {
  images: gql`
    fragment responsiveImageFragment on ResponsiveImage {
      sizes
      src
      srcSet
      webpSrcSet
      alt
    }
  `,
};

export default GraphQL;
