import { TestBed } from '@angular/core/testing';

import { ServicesGQLService } from './services-gql.service';

describe('ServicesGQLService', () => {
  let service: ServicesGQLService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicesGQLService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
