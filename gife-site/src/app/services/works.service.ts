import { ViewportScroller } from '@angular/common';
import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { WorksGQLService, WorkGQLService } from './works-gql.service';
import { ApolloQueryResult } from 'apollo-client';
import { map } from 'rxjs/operators';
import { Subscription } from 'apollo-angular';
import { CommondataService } from '../commondata.service';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class WorksService {
  pageInfo: any;
  allProjects: any = [];
  projectsFilter: any = [];
  isProjectOpen: boolean = false;
  changeProjectEmitter = new EventEmitter<{idProject: string}>();
  changeFilterEmitter = new EventEmitter<string>();
  pageHeaderRef: ElementRef;
  currentFilter: string = 'Tutte le opere';
  dropdownChoices: Object[] = [];
  projectDetailSwiperIndex: number = 0;

  constructor(
    private worksGqlService: WorksGQLService,
    private workGqlService: WorkGQLService,
    private commondataService: CommondataService,
    private router: Router,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller,
  ) {
    this.commondataService.pageHeaderEmitter.subscribe(
      (pageHeaderElement) => {
        this.pageHeaderRef = pageHeaderElement;
      }
    )
  }

  getProjects() {
    return this.worksGqlService.watch()
      .valueChanges
      .subscribe(result => {
        this.allProjects = result
      });
  }

  openProject(idProject) {
    this.workGqlService.watch({
      prjId: idProject
    })
      .valueChanges
      .subscribe(
        result => {
          result.data;
          this.router.navigate(['Works']);
        }
    );

  }

  gotoProject(idProject) {
    this.changeProjectEmitter.emit({idProject});
    this.router.navigate([`opere/${idProject}`]);
  }

  scrollToProject() {
    console.log('this.pageHeaderRef.nativeElement.offsetHeight', this.pageHeaderRef.nativeElement.offsetHeight);
    console.log('this.commondataService.mastheadTopDistance', this.commondataService.mastheadTopDistance);
    console.log('this.commondataService.mastheadTopDistance', this.pageHeaderRef.nativeElement.offsetHeight - this.commondataService.mastheadTopDistance);
    // window.scrollTo(0, this.pageHeaderRef.nativeElement.offsetHeight - this.commondataService.mastheadTopDistance);

    this.viewportScroller.scrollToPosition([0, this.pageHeaderRef.nativeElement.offsetHeight - this.commondataService.mastheadTopDistance]);
  }

  setFilter(filter: string) {
    this.changeFilterEmitter.emit(filter);
  }
}
