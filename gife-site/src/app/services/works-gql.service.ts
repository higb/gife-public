import { Injectable } from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';
import { Subscription } from 'apollo-angular';
import GraphQL from './fragments';


const opereQuery = `
  operePage {
    title
    content
    image {
      responsiveImage(imgixParams: {auto: compress}, sizes: "(max-width: 480px) 480px, (max-width: 960px) 960px, (max-width: 1440px) 1440px, 1920px") {
        ...responsiveImageFragment
      }
      url
    }
  }
`;
const worksQuery = `
  id
  categoria {
    filterName
  }
  title
  location
  committente
  stato
  riferimento
  valore
  description
  coverImage {
    responsiveImage(imgixParams: {fit: crop, w: "640", h: "450", auto: compress}) {
      ...responsiveImageFragment
    }
    url
  }
  gallery {
    responsiveImage(imgixParams: {w: "850", auto: compress}, sizes: "(max-width: 425px) 425px, (max-width: 637px) 637px, 850px") {
      ...responsiveImageFragment
    }
    url
    width
    height
  }
`;

const gifeWorksQuery = gql`
query Works {
  ${opereQuery}
  allWorks {
    ${worksQuery}
  }
  allWorksFilters {
    filterName
  }
}
${GraphQL.fragments.images}
`;

const gifeWorkQuery = gql`
query Work($prjId: ItemId) {
  ${opereQuery}
  work(filter: {id: {eq: $prjId}}) {
    ${worksQuery}
  }
}
${GraphQL.fragments.images}
`;

@Injectable({
  providedIn: 'root'
})
export class WorksGQLService extends Query {
  document = gifeWorksQuery;
}

@Injectable({
  providedIn: 'root'
})
export class WorkGQLService extends Query {
  document = gifeWorkQuery;
}
