import { Injectable } from '@angular/core';

import { faMapMarkerAlt, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  faMapMarkerAlt = faMapMarkerAlt;
  faPhoneAlt = faPhoneAlt;
  faPaperPlane = faPaperPlane;

  truncateString(str) {
    const sliceBoundary = str => str.substr(0, str.lastIndexOf(" "));
    const truncate = (n, useWordBoundary) => 
          str.length <= n ? str : `${ useWordBoundary 
            ? sliceBoundary(str.slice(0, n - 1))
            : str.substr(0, n - 1)}&hellip;`;
    return { full: str,  truncate };
  }

  iconSetup(property: string) {
    let icon: IconDefinition;
    switch(property.toLowerCase()) {
      case 'indirizzo':
        icon = this.faMapMarkerAlt;
        break;
      case 'mail':
        icon = this.faPaperPlane;
        break;
      case 'telefono':
        icon = this.faPhoneAlt;
        break;
    }

    return icon
  }
}
