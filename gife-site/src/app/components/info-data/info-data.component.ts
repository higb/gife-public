import { Component, OnInit, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-info-data',
  templateUrl: './info-data.component.html',
  styleUrls: ['./info-data.component.scss']
})
export class InfoDataComponent implements OnInit, OnDestroy {
  @Input() infos: {};
  @Input() titleLink: string;

  isCalled: boolean = false;

  constructor() { }

  ngOnInit() {}

  ngOnDestroy(): void {
    console.warn(this.infos)
  }

  public onIntersection({ target, visible }: { target: Element; visible: boolean }): void {
    if (visible && !this.isCalled) {
      Array.prototype.map.call(this.infos, (info) => this.startCount(info));
      this.isCalled = true;
    }
  }


  private startCount(info) {
    if (info.qualityTitle) {
      const symbol = info.qualityTitle.match(/(\+|%)/g) ? [...info.qualityTitle].pop() : '';
      const qualityStringNumber = info.qualityTitle.replace(/(\+|%)/, '');
      const totalDuration = 4000;

      if (!isNaN(qualityStringNumber)) {
        let qualityNumber = parseInt(qualityStringNumber);
        const limit = qualityNumber;
        let count = 0;
        const a = 1.04;
        let summatory = 0;
        const intervalArray: Array<number> = [];

        for (let i = 0; i <= limit; i++) {
          let newInterval = (totalDuration-summatory) / ( (a - Math.pow(a, -(limit-1))) / (a-1));
          summatory += newInterval;
          intervalArray.unshift(newInterval);
        }

        const setInfo = () => {
          if (count <= limit) {
            info.qualityTitle = `${count}${symbol}`;
            setTimeout(setInfo, intervalArray[count]);
            count++;
          }
        }

        setInfo();

      }
    }
  }

}
