import { Component, Input, OnInit } from '@angular/core';
import { CommondataService } from '../../commondata.service';
import { UtilsService } from 'src/app/utils.service';

import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  text: string;
}

const customSwiperConfig: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 1,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  spaceBetween: 50,
  autoplay: {
    delay: 5000,
  },
  //slidesOffsetBefore: 300,
  //centeredSlides: true,
  //initialSlide: 1
}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  @Input() newsList: {};
  
  customSwiperConfig = customSwiperConfig;

  constructor(private apollo: Apollo, public commondataService: CommondataService, public utilsService: UtilsService) { }

  ngOnInit() {

  }

  tiles: Tile[] = [
    {text: 'One', cols: 3, rows: 1, color: 'lightblue'},
    {text: 'Two', cols: 1, rows: 2, color: 'lightgreen'},
    {text: 'Three', cols: 1, rows: 1, color: 'lightpink'},
    {text: 'Four', cols: 2, rows: 1, color: '#DDBDF1'},
  ];

}
