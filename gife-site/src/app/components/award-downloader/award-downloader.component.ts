import { Component, Input, OnInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';


@Component({
  selector: 'app-award-downloader',
  templateUrl: './award-downloader.component.html',
  styleUrls: ['./award-downloader.component.scss']
})
export class AwardDownloaderComponent implements OnInit {
  @Input() awards: []

  tabletBP: boolean;

  constructor(
    private bpObserver: BreakpointObserver
  ) { }

  ngOnInit() {
    const bp = '(min-width: 55rem)';

    this.bpObserver.observe(bp)
      .subscribe(result => {
        this.tabletBP = result.matches;
      });
  }

}
