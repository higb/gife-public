import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AwardDownloaderComponent } from './award-downloader.component';

describe('AwardDownloaderComponent', () => {
  let component: AwardDownloaderComponent;
  let fixture: ComponentFixture<AwardDownloaderComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AwardDownloaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AwardDownloaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
