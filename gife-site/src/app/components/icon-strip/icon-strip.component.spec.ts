import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { IconStripComponent } from './icon-strip.component';

describe('IconStripComponent', () => {
  let component: IconStripComponent;
  let fixture: ComponentFixture<IconStripComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ IconStripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconStripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
