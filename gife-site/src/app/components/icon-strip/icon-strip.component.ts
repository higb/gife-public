import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CommondataService } from '../../commondata.service';

@Component({
  selector: 'app-icon-strip',
  templateUrl: './icon-strip.component.html',
  styleUrls: ['./icon-strip.component.scss']
})
export class IconStripComponent implements OnInit {
  @Input() stripData: any;
  @Output() modalTrigger = new EventEmitter();

  constructor(
    public commondataService: CommondataService,
    public router: Router,
    ) { }

  ngOnInit() {}

}
