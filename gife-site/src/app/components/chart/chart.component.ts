import { Subscription } from 'rxjs';
import { Component, Input, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit,AfterViewInit, OnDestroy {
  @Input() data: {};
  @Input() height: number;
  @Input() title: string;

  @ViewChild('frappeChart') frappeChart:ElementRef;

  bpObserverSubscription: Subscription;
  currentBreakpoint: string = '';

  constructor(
    private bpObserver: BreakpointObserver,
  ) { }

  ngOnInit(): void {
    const bpArray = ['(min-width: 42.5rem)', '(min-width: 67.5rem)', '(min-width: 80rem)'];

    this.bpObserverSubscription = this.bpObserver.observe(bpArray)
        .subscribe(result => {
          switch (true) {
            case result.breakpoints[bpArray[0]] && !result.breakpoints[bpArray[1]] || result.breakpoints[bpArray[1]] && !result.breakpoints[bpArray[2]]:
              this.currentBreakpoint = 'tablet';
              break;
            case result.breakpoints[bpArray[2]]:
              // desktop
              this.currentBreakpoint = 'desktop';
              break;
            default:
              // mobile 0 -> 42.5rem
              this.currentBreakpoint = 'mobile';
          }
      });
  }

  ngAfterViewInit(): void {
    console.log(this.frappeChart);
  }

  ngOnDestroy(): void {
    this.bpObserverSubscription.unsubscribe();
  }

}
