import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ClientsSwiperComponent } from './clients-swiper.component';

describe('ClientsSwiperComponent', () => {
  let component: ClientsSwiperComponent;
  let fixture: ComponentFixture<ClientsSwiperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsSwiperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsSwiperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
