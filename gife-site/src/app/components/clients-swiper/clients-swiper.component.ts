import { Component, OnInit, Input } from '@angular/core';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

interface ClientsData {
  partnersTitle: '',
  partnersList: {}
}

@Component({
  selector: 'app-clients-swiper',
  templateUrl: './clients-swiper.component.html',
  styleUrls: ['./clients-swiper.component.scss']
})
export class ClientsSwiperComponent implements OnInit {
  @Input() clientData: ClientsData;
  @Input() slidesHeight: number[];

  maxLogoHeight: number = 70;

  desktopSize: number; // TODO: try to use "Enum" -> type enum Sizes { desktop = 1000, tablet = 800, mobile = 400 }
  tabletSize: number;
  mobileSize: number;

  clientsSwiperConfig: SwiperConfigInterface = {
    slidesPerColumnFill: 'row',
    // spaceBetween: 5,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    },
    slidesPerView: 1,
    slidesPerColumn: 3,
    breakpoints: {
      640: {
        slidesPerView: 2,
        slidesPerColumn: 2,
        slidesPerGroup: 2,
      },
      768: {
        slidesPerView: 4,
        slidesPerColumn: 2,
        slidesPerGroup: 2,
      },
      1024: {
        slidesPerView: 6,
        slidesPerColumn: 2,
        slidesPerGroup: 2,
      }
    }
  }

  constructor() {}

  ngOnInit() {
    const [ desktopSize, tabletSize, mobileSize ] = this.slidesHeight;
    this.desktopSize = desktopSize;
    this.tabletSize = tabletSize;
    this.mobileSize = mobileSize;
  }

  getBaseLog(x, y) {
    return Math.log(y) / Math.log(x);
  }
}
