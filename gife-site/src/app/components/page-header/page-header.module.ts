import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  SprkBoxModule,
  SprkHeadingModule,
  SprkTextModule,
} from "@sparkdesignsystem/spark-angular";

import { PageHeaderComponent } from './page-header.component';

@NgModule({
  declarations: [
    PageHeaderComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,

    SprkBoxModule,
    SprkHeadingModule,
    SprkTextModule,
  ],
  exports: [
    PageHeaderComponent,
  ],
})
export class PageHeaderModule {}
