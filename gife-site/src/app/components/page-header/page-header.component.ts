import { Router, ActivatedRoute } from '@angular/router';
import { subscribe } from 'graphql';
import { CommondataService } from './../../commondata.service';
import { WorksService } from 'src/app/services/works.service';
import { Component, OnInit, Input, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { faArrowDown, faTimes, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { filter } from 'rxjs/operators';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';

interface IResponsiveImage {
  responsiveImage: {
    sizes: string;
    src: string;
    srcSet: string;
    webpSrcSet: string;
    alt: string;
  }
  url: string;
}

@Component({
  selector: 'app-page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() responsiveImageUrls: IResponsiveImage;
  @Input() pageTitle: string;
  @Input() pageDescription?: string;
  @Input() filters?: Array<number>;
  @Input() currentFilter?: string;
  @Input() docDownload?: string;
  @Input() centerAlign?: boolean;
  @Input() bigQuote?: boolean;
  @Input() autoreCitazione?: string;

  faArrowDown: IconDefinition = faArrowDown;
  faTimes: IconDefinition = faTimes;
  showDropdown: boolean = false;
  currentUrl: string = this.router.url;

  @ViewChild('pageHeaderReference') pageHeaderReference: ElementRef;
  @ViewChild('filterDropdown') filterDropdown: ElementRef;

  dropdownVisibility: boolean = true;
  bpObserverSubscription: Subscription;

  constructor(
    public commondataService: CommondataService,
    public worksService: WorksService,
    private bpObserver: BreakpointObserver,
    public router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    if (this.currentUrl.includes('opere')) {
      const bpArray = ['(min-width: 42.5rem)', '(min-width: 67.5rem)', '(min-width: 80rem)'];

      this.filters.length && this.worksService.dropdownChoices.length < 1 && this.filters.map((filter: Object) => {
        if (this.worksService.dropdownChoices.length === 0) {
          this.worksService.dropdownChoices.push({
            content: {
              title: 'Tutte le opere'
            },
            value: 'Tutte le opere',
            isDefault: true,
            active: 'Tutte le opere' === this.currentFilter || this.currentFilter === '' ? true : false
          });
        }

        const choice = {
          content: {
            title: filter['filterName'],
          },
          value: filter['filterName'],
          active: filter['filterName'] === this.currentFilter ? true : false
        }

        this.worksService.dropdownChoices.push(choice);
      });

      this.bpObserverSubscription = this.bpObserver.observe(bpArray)
        .subscribe(result => {
          switch (true) {
            case result.breakpoints[bpArray[0]] && !result.breakpoints[bpArray[1]] || result.breakpoints[bpArray[1]] && !result.breakpoints[bpArray[2]]:
              this.dropdownVisibility = true;
              break;
            case result.breakpoints[bpArray[2]]:
              // desktop
              this.dropdownVisibility = false;
              break;
            default:
              // mobile 0 -> 42.5rem
              this.dropdownVisibility = true;
          }
      });
    }
  }

  ngAfterViewInit(): void {
    this.commondataService.pageHeaderEmitter.emit(this.pageHeaderReference);
  }

  ngOnDestroy(): void {
    if (this.currentUrl.includes('opere')) {
      this.bpObserverSubscription.unsubscribe();
    }
  }

}
