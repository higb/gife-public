import { BreakpointObserver } from '@angular/cdk/layout';
import { DOCUMENT } from '@angular/common';
import { Component, Input, OnInit, AfterViewInit, ViewChildren, ViewChild, QueryList, ElementRef, Renderer2, AfterContentChecked, HostListener, Inject } from '@angular/core';
import { faAsterisk, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-bullet-posts',
  templateUrl: './bullet-posts.component.html',
  styleUrls: ['./bullet-posts.component.scss']
})
export class BulletPostsComponent implements OnInit, AfterViewInit, AfterContentChecked {
  @Input() bulletElements: string[];

  @ViewChildren('refBulletItem') bulletItems: QueryList<ElementRef>;
  @ViewChild('refBulletContainer', { read: ElementRef }) BulletContainer: ElementRef;
  // @ViewChildren('refBulletContainer', { read: ElementRef }) BulletContainerDren: QueryList<ElementRef>;

  bulletsContainerHeight: number = 1000;
  faAsterisk: IconDefinition = faAsterisk;
  grid: Element;
  rowGap: number;
  rowHeight: number;


  /* Resize all the grid items on the load and resize events */
  @HostListener('window:resize', ['$event'])
  masonryResize(event) {
    this.resizeAllMasonryItems();
  }

  constructor(
    private bpObserver: BreakpointObserver,
    private renderer: Renderer2,
    @Inject(DOCUMENT) private document: Document
  ) { }

  ngOnInit(): void {

  }

  ngAfterContentChecked(): void {

  }

  ngAfterViewInit(): void {
    this.grid = this.document.querySelector('.c-bullet-posts__item-container'),
    this.rowGap = parseInt(getComputedStyle(this.grid).getPropertyValue('grid-row-gap')),
    this.rowHeight = parseInt(getComputedStyle(this.grid).getPropertyValue('grid-auto-rows'));

    console.log('PARAMETERS', this.rowGap, this.rowHeight);
    this.resizeAllMasonryItems();
  }

/**
 * Set appropriate spanning to any masonry item
 *
 * Get different properties we already set for the masonry, calculate
 * height or spanning for any cell of the masonry grid based on its
 * content-wrapper's height, the (row) gap of the grid, and the size
 * of the implicit row tracks.
 *
 * @param item Object A brick/tile/cell inside the masonry
 */
resizeMasonryItem(item){
  /* Get the grid object, its row-gap, and the size of its implicit rows */
  /*
   * Spanning for any brick = S
   * Grid's row-gap = G
   * Size of grid's implicitly create row-track = R
   * Height of item content = H
   * Net height of the item = H1 = H + G
   * Net height of the implicit row-track = T = G + R
   * S = H1 / T
   */
  const rowSpan = Math.ceil((item.querySelector('.c-bullet-posts__content').getBoundingClientRect().height+this.rowGap)/(this.rowHeight+this.rowGap));

  /* Set the spanning as calculated above (S) */
  this.renderer.setStyle(item, 'gridRowEnd', `span ${rowSpan}`);
}

  /**
   * Apply spanning to all the masonry items
   *
   * Loop through all the items and apply the spanning to them using
   * `resizeMasonryItem()` function.
   *
   * @uses resizeMasonryItem
   */
  resizeAllMasonryItems() {
    // Get all item class objects in one list
    /*
    * Loop through the above list and execute the spanning function to
    * each list-item (i.e. each masonry item)
    */

    this.bulletItems.forEach(child => {
      this.resizeMasonryItem(child.nativeElement);
    })
  }

}
