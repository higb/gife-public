import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BulletPostsComponent } from './bullet-posts.component';

describe('BulletPostsComponent', () => {
  let component: BulletPostsComponent;
  let fixture: ComponentFixture<BulletPostsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BulletPostsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BulletPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
