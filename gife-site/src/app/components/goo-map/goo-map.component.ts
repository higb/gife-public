import { subscribe } from 'graphql';
import { CommondataService } from 'src/app/commondata.service';
import { ActivatedRoute } from '@angular/router';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit, Input, QueryList, ViewChildren } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { MapGeocoder } from './map-geocoder';
import { ViewportScroller } from '@angular/common';
import { GoogleMap } from '@angular/google-maps';

const iconSvg = '%3Csvg width="2160" height="516" xmlns="http://www.w3.org/2000/svg"%3E%3Cpath d="M172.3 501.7C27 291 0 269.4 0 192 0 86 86 0 192 0s192 86 192 192c0 77.4-27 99-172.3 309.7-9.5 13.7-29.9 13.7-39.4 0zM192 272c44.2 0 80-35.8 80-80s-35.8-80-80-80-80 35.8-80 80 35.8 80 80 80z" fill="%23ea3423"/%3E%3Cpath d="M506.8 124.2c18.7-.8 37.3 1.4 55.3 6.5v24.1c-15.5-2.2-31.1-3.4-46.7-3.6-19.6 0-23.2 10.9-23.2 43.7 0 33.8 2.8 42 21.1 42 7.4.1 14.5-3.1 19.4-8.7V208h-14.8v-19.5c8.3-3.8 19.9-4.5 33.1-4.5 4.2 0 8.3 0 12.9.4v80.5h-21.8l-8.7-9.3c-5.7 6.3-11 9.5-26.8 10.3-39.6 2-45.4-31.8-45.4-71.3-.2-41.4 7.8-70.4 45.6-70.4zM580.9 125.8h31.2v139h-31.2v-139zM630.3 241c-.3-2.3 1.2-4.4 3.5-4.7.3 0 .7-.1 1 0h18.6c2.3-.3 4.4 1.2 4.8 3.5.1.4.1.8 0 1.3v19.4c0 2.3-1.8 4.2-4.1 4.2H634.9c-2.3.2-4.3-1.5-4.5-3.8V241h-.1zM678.4 156.6c0-24.1 17.3-31.3 32.5-31 0 0 45 .8 61.6 3v26h-53c-9.9 0-9.9 5.5-9.9 10.6V181l54 1.9v25.8l-54 1.7v54.3h-31.2V156.6zM786.2 156.1c0-26.2 19.2-30.6 32.5-30.4 0 0 45 .8 61.6 3v26h-53c-9.9 0-9.9 5.5-9.9 10.6v15.8l54 1.9v25.8l-53.9 1.7v15.8c0 10.1 6.3 9.9 11.8 9.9h51.1V262c-14.6 1.9-59.7 3.2-59.7 3.2-12.5.6-34.4-2.1-34.4-29.5l-.1-79.6zM895.2 241c-.3-2.3 1.2-4.4 3.5-4.7.3 0 .7-.1 1 0h18.6c2.3-.3 4.4 1.2 4.8 3.5.1.4.1.8 0 1.3v19.4c0 2.3-1.8 4.2-4.1 4.2H899.8c-2.3.2-4.3-1.5-4.5-3.8V241h-.1zM1036.1 124.2c23 0 33.6 1.7 49.2 6.8v24.3c-13.5-1.7-27.1-2.6-40.6-2.8-18.1 0-25 2.1-25 42.7 0 31.2 4.2 42.4 25.3 42.4 19.8 0 40.5-1.7 40.5-1.7v24.3c-12.1 4.2-25.7 6.1-49.2 5.7-35.1-.4-47.6-25.9-47.6-71.1-.2-44.4 9.5-70.6 47.4-70.6zM1150.7 124.7c32.9 0 56.6 6.3 56.6 71.4 0 61.4-22.4 69.4-56.6 69.4s-56.6-6.2-56.6-69.4c-.1-65.1 24.6-71.4 56.6-71.4zm0 112.7c19.8 0 25.5-7 25.5-41 0-38.4-6.1-43.2-25.5-43.2s-25.5 3.4-25.5 43.2c0 33.8 4 40.9 25.5 40.9v.1zM1221.2 161.2c0-27.5 16.4-36.3 37.6-36.7 23.6-.6 43.7 1.7 56 5.9v24.3c-12.1-1.1-36.7-1.9-46.2-1.9-11 0-16.4.6-16.4 11.2v4.8c0 8.7 2.8 11.4 13.3 11.4h20.2c28.1 0 34.2 23.2 34.2 38.4v8c0 33.4-19.8 39.7-38.6 39.7-29.2 0-39.5-.8-55.9-6.1v-24.3c7.6.8 31 2.1 48.8 2.1 8 0 14.4-1.7 14.4-10.1v-5.5c0-6.8-2.1-11-11.4-11h-19.6c-34.8 0-36.7-27.5-36.7-38.4l.3-11.8zM1324.8 125.8h105.1v28.3h-37.6v110.7h-31.2V154h-36.5v-28.2h.2zM1442.8 125.8h61.4c23 0 40.1 10.1 40.1 43.9s-12.9 38.4-21.5 41.4l25.3 53.8h-35.3l-20.9-51.1-18.1-2.7v53.6h-31.2v-139l.2.1zm57.7 61.4c9.5 0 12.1-3.6 12.1-17.5 0-12.5-3.8-16.9-12.1-16.9h-26.4v34.4h26.4zM1560.3 125.8h31.2V204c0 25.7 4.2 33.4 23.2 33.4 17.3 0 22-8.7 22-33.4v-78.2h31.3V204c0 61.4-30.4 61.8-53.4 61.8-25.8 0-54.3 1.5-54.3-61.8v-78.2zM1679.9 236.4l59.5-82.5h-59.5v-28.2h96.5V154l-55.3 82.3h55.3v28.3h-96.6l.1-28.2zM1790.9 125.8h31.2v139h-31.2v-139zM1894.5 124.7c32.9 0 56.6 6.3 56.6 71.4 0 61.4-22.4 69.4-56.6 69.4s-56.6-6.2-56.6-69.4c0-65.1 24.7-71.4 56.6-71.4zm0 112.7c19.8 0 25.5-7 25.5-41 0-38.4-6.1-43.2-25.5-43.2s-25.5 3.4-25.5 43.2c.1 33.8 4 40.9 25.5 40.9v.1zM1966.9 125.8h31.6l48.2 81.4v-81.4h31.1v139h-31.2l-48.3-80.2v80.2H1967l-.1-139zM2095.6 125.8h31.2v139h-31.2v-139z"/%3E%3C/svg%3E';

const mapMarker = {
  url: 'data:image/svg+xml,' + iconSvg,
  fillColor: '#EA3423',
  fillOpacity: 1,
  strokeWeight: 0,
  rotation: 0,
  scale: 0.1,
}
@Component({
  selector: 'app-goo-map',
  templateUrl: './goo-map.component.html',
  styleUrls: ['./goo-map.component.scss']
})
export class GooMapComponent implements OnInit, OnDestroy, AfterViewInit {
  apiLoaded: Observable<boolean>;
  test: Observable<any>;
  options: google.maps.MapOptions = {
    // center: locations.a,
    zoom: 18,
    disableDefaultUI: true,
    styles: [
      {
        "featureType": "administrative",
        "elementType": "all",
      },{
        "featureType": "poi",
        "elementType": "all",
      },{
        "featureType": "poi.business",
        "elementType": "all",
        "stylers": [
          { "visibility": "off" }
        ]
      },

    ]
  };
  dynamicHeight: number = 500;
  bpObserverSubscription: Subscription;
  markerOptions: google.maps.MarkerOptions = {
    draggable: false,
    icon: mapMarker,
  };

  // @ViewChildren('googleMapRef') googleMapRef: QueryList<ElementRef>;

  @ViewChild(GoogleMap, { static: false }) Gmap: GoogleMap;

  constructor(
    private httpClient: HttpClient,
    private bpObserver: BreakpointObserver,
    public route: ActivatedRoute,
    public commondataService: CommondataService,
    public geocoder: MapGeocoder,
    private viewportScroller: ViewportScroller,
  ) {
      if (!this.commondataService.loadedAPI) {
        this.apiLoaded = httpClient.jsonp(`https://maps.googleapis.com/maps/api/js?key&map_ids=fa7b4e4aec53701`, 'callback')
            .pipe(
              map(() => {
                this.commondataService.loadedAPI = true;
                this.markerOptions.icon['scaledSize'] = new google.maps.Size(216, 51.6);
                this.markerOptions.icon['anchor'] = new google.maps.Point(20, 51.6);
                return true
              }),
              catchError(() => of(false)),
            );
      }
  }

  ngOnInit(): void {
    const bpArray = ['(min-width: 42.5rem)', '(min-width: 67.5rem)', '(min-width: 80rem)'];

    this.bpObserverSubscription = this.bpObserver.observe(bpArray)
      .subscribe(result => {

      switch (true) {
        case result.breakpoints[bpArray[0]] && !result.breakpoints[bpArray[1]]:
          this.dynamicHeight = 400;
          break;
        case result.breakpoints[bpArray[1]] && !result.breakpoints[bpArray[2]]:
          this.dynamicHeight = 500;
          break;
        case result.breakpoints[bpArray[2]]:
          // desktop
          this.dynamicHeight = 600;
          break;
        default:
          // mobile 0 -> 42.5rem
          this.dynamicHeight = 300;
      }

      console.log('RESULT', result);

    });

  }

  ngAfterViewInit(): void {
    if (this.route.snapshot.fragment) {
      setTimeout(() => {
        this.viewportScroller.scrollToAnchor('map');
        console.warn('SCROLL TO ANCHOR')
      }, 500)
    }
  }

  ngOnDestroy(): void {
    this.bpObserverSubscription.unsubscribe();
  }


  panCenter() {
    console.log('im panning');
    this.Gmap.panBy(50, 0);
  }
}
