import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ActionCallerComponent } from './action-caller.component';

describe('ActionCallerComponent', () => {
  let component: ActionCallerComponent;
  let fixture: ComponentFixture<ActionCallerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionCallerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionCallerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
