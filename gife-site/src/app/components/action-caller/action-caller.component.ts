import { Component, OnInit, Input } from '@angular/core';

interface ActionCaller {
  jobImage: {
    url: ''
  },
  jobTitle,
  jobDescription,
  jobMail
}
@Component({
  selector: 'app-action-caller',
  templateUrl: './action-caller.component.html',
  styleUrls: ['./action-caller.component.scss']
})
export class ActionCallerComponent implements OnInit {
  @Input() actionInfo: ActionCaller;

  constructor() { }

  ngOnInit() {
  }

}
