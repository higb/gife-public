import { CommondataService } from './../../commondata.service';
import { Component, Input, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils.service';
import { ViewportScroller } from '@angular/common';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  @Input() heading: string;
  @Input() paragraph: string;
  @Input() isList: boolean;
  @Input() elementsList: [];

  textList: [];

  constructor(
    public utilsService: UtilsService,
    public commondataService: CommondataService,
    public viewportScroller: ViewportScroller,
  ) { }

  ngOnInit(): void {
  }

}
