import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  SprkLinkDirectiveModule,
  SprkHeadingModule,
  SprkTextModule
} from "@sparkdesignsystem/spark-angular";

import { TextComponent } from './text.component';

@NgModule({
  declarations: [
    TextComponent,
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,

    SprkLinkDirectiveModule,
    SprkHeadingModule,
    SprkTextModule,
  ],
  exports: [
    TextComponent,
  ],
})
export class TextModule {}
