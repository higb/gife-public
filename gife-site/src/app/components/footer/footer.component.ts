import { ViewportScroller } from '@angular/common';
import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { CommondataService } from '../../commondata.service';
import { UtilsService } from 'src/app/utils.service';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faPhoneAlt } from '@fortawesome/free-solid-svg-icons';
import { faPaperPlane } from '@fortawesome/free-solid-svg-icons';

import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { Router, ActivatedRoute } from '@angular/router';

interface Contacts {
  ragioneSociale: string;
  partitaIva: string;
  codiceFiscale: string;
  copyright: string;
  contatti: Array<{
    descrizioneContatto: string;
    iconaContatto: string;
    titoloContatto: string;
  }>;
  privacy: string;
  privacyFile: {
    url: string;
  }

}

interface globalUI {
  data: {
    header: {
      brandMenuLogo: {
        url: string
      }
    },
    allSocialProfiles: []
  }
}

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit, AfterViewInit {
  @Input() contacts: Contacts;
  @Input() menuItems: [];
  @Input() siteData: globalUI;
  @Input() bigLogoUrl: string;

  @ViewChild('cFooter') cFooterElement: ElementRef;

  faMapMarkerAlt = faMapMarkerAlt;
  faPhoneAlt = faPhoneAlt;
  faPaperPlane = faPaperPlane;
  faLinkedin = faLinkedin;
  faFacebookSquare = faFacebookSquare;

  slicedMenuVoices = [];

  currentYear = new Date();

  constructor(
    public commondataService: CommondataService,
    public utilsService: UtilsService,
    public router: Router,
    public route: ActivatedRoute,
    public viewportScroller: ViewportScroller,
  ) { }

  ngOnInit() {
    let menuVoices = this.menuItems;
    let slicedMenuVoices: any = [];
    let i, chunk = Math.ceil(menuVoices.length / 2);

    for (i = 0; i < menuVoices.length; i += chunk) {
      let chunkArray: any = menuVoices.slice(i, i+chunk);
      slicedMenuVoices.push(chunkArray);
    }

    this.slicedMenuVoices = slicedMenuVoices;
  }

  ngAfterViewInit() {
    this.commondataService.sendElementRef.emit(this.cFooterElement);
  }
}
