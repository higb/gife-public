import { CommondataService } from 'src/app/commondata.service';
import { subscribe } from 'graphql';
import { ActivatedRoute } from '@angular/router';
import { Component, Inject, OnInit, Input, ViewChild, ElementRef, AfterViewInit, Renderer2, ViewChildren, QueryList, HostListener } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-split-view',
  templateUrl: './split-view.component.html',
  styleUrls: ['./split-view.component.scss'],
  providers: [
    { provide: Window, useValue: window },
  ],
})
export class SplitViewComponent implements OnInit, AfterViewInit {
  @Input() content: any;

  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    console.log('******** LISTENER INNER HEIGHT',window.innerHeight);
  }

  currentFragment: string;
  rect: any;
  tabletBP: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private renderer: Renderer2,
    private viewportScroller: ViewportScroller,
    @Inject(Window) private _window: Window,
    private commondataService: CommondataService,
    private bpObserver: BreakpointObserver,
  ) { }

  ngOnInit(): void {
    const bp = '(min-width: 55rem)';

    this.bpObserver.observe(bp)
      .subscribe(result => {
        this.tabletBP = result.matches;
      });
  }

  ngAfterViewInit() {
    this.route.fragment.subscribe(frag => {
      console.log('FRAG', frag)
      // this.prova.nativeElement.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
      this.currentFragment = frag;
      console.log('current Frag', this.currentFragment);

      console.log(this.content.serviceName.replace(new RegExp(' ', 'g'), '_').toLowerCase() === this.currentFragment)

      if (this.content.serviceName.replace(new RegExp(' ', 'g'), '_').toLowerCase() === this.currentFragment) {
        // console.log('GET CLIENT', this.test2.first.nativeElement.getBoundingClientRect().top)
        console.log('INERHEIGHT / 2', (-window.innerHeight / 2));

        console.log('this.commondataService.mastheadHeight', this.commondataService.mastheadHeight);

        setTimeout(() => {
            this.viewportScroller.setOffset([0, this.commondataService.mastheadHeight]);
            this.viewportScroller.scrollToAnchor(this.currentFragment);
          },
          500
        )

      }
    });
  }
}
