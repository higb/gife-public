import { WorksService } from 'src/app/services/works.service';
import { CommondataService } from './../../commondata.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { Project } from '../../interfaces/types';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { faTimes, IconDefinition } from '@fortawesome/free-solid-svg-icons';

const projectDetailSwiperConfig: SwiperConfigInterface = {
  centeredSlides: true,
  spaceBetween: 5,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  autoplay: {
    delay: 5000,
  },
};

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit {
  @Input() currentProject: Project;

  faTimes: IconDefinition = faTimes;

  projectDetailSwiperConfig = projectDetailSwiperConfig
  project = { id: '' };
  test: any;

  constructor(
    private route: ActivatedRoute,
    public router: Router,
    public commondataService: CommondataService,
    public worksService: WorksService) { }

  ngOnInit(): void {}

  ratio({ height, width }): number {
    return height / width
  }

}
