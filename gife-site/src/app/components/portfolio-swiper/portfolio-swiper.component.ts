import { Component, ViewChild, ContentChildren, ElementRef, OnInit, Input, AfterContentInit, AfterViewInit, QueryList } from '@angular/core';
import { SwiperConfigInterface, SwiperDirective, SwiperComponent } from 'ngx-swiper-wrapper';
import { CommondataService } from 'src/app/commondata.service';
import { WorksService } from 'src/app/services/works.service';

// declare var SwiperModuleModule: any;

interface IProject {
  title: string;
  coverImage: {
    url: string;
  };
  image: string;
}

@Component({
  selector: 'app-portfolio-swiper',
  templateUrl: './portfolio-swiper.component.html',
  styleUrls: ['./portfolio-swiper.component.scss']
})
export class PortfolioSwiperComponent implements OnInit, AfterContentInit, AfterViewInit {
  @Input() prjElements: IProject[];
  @Input() sideTitle: any;
  @Input() titleLink: string;
  @Input() customSwiperConfig: {};
  @Input() customHeight: string;
  @Input() stepContainer: boolean;
  @Input() decorations: boolean;
  @Input() underlined: boolean;
  @Input() hover: boolean;
  @Input() bigLogoUrl: string;
  @Input() bigArrow: boolean;

  @ViewChild('portfolioReference') portfolioReference: ElementRef;
  // @ContentChild('mainSwiperReference', { static: false }) mySwiperReference: ElementRef;

  // @ContentChild('sideTitleContent') sideTitleContentChild: ElementRef;
  // @ContentChildren('sideTitleContent') sideTitleContentChild: QueryList<ElementRef>;

  remappedPrjElements = [];

  constructor(
    private commondataService: CommondataService,
    public worksService: WorksService
  ) { }

  ngOnInit() {
    for (const element of this.prjElements) {
      let currentImageProperty = Object.getOwnPropertyNames(element).filter(x => x.match('Image'))[0];
      element.image = element[currentImageProperty];
    }

    this.remappedPrjElements = this.prjElements;
  }

  ngAfterContentInit() {

  }

  ngAfterViewInit() {

  }

  scrollPage() {
    console.log('mastheadTopDistance', this.commondataService.mastheadTopDistance);
    window.scrollTo(0, this.portfolioReference.nativeElement.offsetHeight - (35 + this.commondataService.mastheadTopDistance));
  }
}
