import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PortfolioSwiperComponent } from './portfolio-swiper.component';

describe('PortfolioSwiperComponent', () => {
  let component: PortfolioSwiperComponent;
  let fixture: ComponentFixture<PortfolioSwiperComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PortfolioSwiperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PortfolioSwiperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
