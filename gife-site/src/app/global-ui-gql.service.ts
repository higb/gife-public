import {Injectable} from '@angular/core';
import {Query} from 'apollo-angular';
import gql from 'graphql-tag';

const uiQuery = gql`
query {
  header {
    brandEvent
    brandEventDescription
    brandName
    brandLogo {
      url
    }
    brandMenuLogo {
      url
    }
  }
  footer {
    codiceFiscale
    partitaIva
    ragioneSociale
    copyright
    privacyFile {
      url
    }
    privacy
    testoModaleCookie
    titoloModaleCookie
  }
  contact {
    contatti {
      descrizioneContatto
      iconaContatto
      titoloContatto
      coordinate {
        latitude
        longitude
      }
    }
  }
  allSocialProfiles {
    url
    slug
    profileType
    enable
  }
}
`;

@Injectable({
  providedIn: 'root'
})
export class GlobalUIGQLService extends Query {

  document = uiQuery;
  
}
