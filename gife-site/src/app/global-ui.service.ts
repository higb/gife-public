import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { GlobalUIGQLService } from './global-ui-gql.service';


@Injectable({
  providedIn: 'root'
})
export class GlobalUIService {
  globalUI: any;

  constructor(private globalUIGQLService: GlobalUIGQLService) { }

}
