import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { WorksResolver } from './works-resolver.service';

const routes: Routes = [
  { path: 'home', component: HomepageComponent, data: {brandicon: ''} },
  { path: 'servizi', loadChildren: () => import('./pages/servizi/servizi.module').then(m => m.ServiziModule) },
  { path: 'azienda', loadChildren: () => import('./pages/azienda/azienda.module').then(m => m.AziendaModule) },
  { path: 'opere', loadChildren: () => import('./pages/works/works.module').then(m => m.WorksModule), resolve: { projects: WorksResolver } },
  { path: 'opere/:idProject', loadChildren: () => import('./pages/works/works.module').then(m => m.WorksModule), data: { openProject: true } },
  { path: 'contatti', loadChildren: () => import('./pages/contatti/contatti.module').then(m => m.ContattiModule) },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: HomepageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    anchorScrolling: 'enabled',
    onSameUrlNavigation: 'reload',
    // scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'legacy'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
