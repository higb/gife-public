import { Component, OnInit, AfterViewInit, ViewChild, ViewChildren, ElementRef, AfterContentInit, AfterViewChecked, QueryList, HostListener, Renderer2, Host, HostBinding } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Globals } from './globals';

import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';
import { DomSanitizer } from '@angular/platform-browser';


import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GlobalUIGQLService } from './global-ui-gql.service';
import { HomedataService } from './services/homedata.service';
import { CommondataService } from './commondata.service';

import { Router, ActivatedRoute, Data } from '@angular/router';

import { faLinkedin } from '@fortawesome/free-brands-svg-icons';
import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import { GlobalUIService } from './global-ui.service';
import { SprkMastheadAccordionItemModule } from '@sparkdesignsystem/spark-angular';

const SiteClient = require('datocms-client').SiteClient;
const client = new SiteClient('');


interface MenuVoices {
  data: {
    attributes: {
      label: '';
    }
  }
}

interface MenuList {
  data: {}
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  title = 'Tours of Heroes';
  pageContent = {};
  menuList: MenuList;
  modalVisible: boolean = false;
  modalTitle: string;
  modalFragment: boolean;
  modalContent: string | HTMLElement = '';
  modalType: string = 'info';
  topDistance: number = 45;
  mastHeadTarget: HTMLElement;
  narrowLinks: object[] = [];

  httpOptions = {
    headers: new HttpHeaders({ 'accept': 'application/json', 'Authorization': 'Bearer 05a16871be2258f8df75ac17d73c46' })
  };
  testgql = undefined;

  globalUI: any = {};
  header_content: any = {};


  rates: any[];
  loading = true;
  error: any;

  faLinkedin = faLinkedin; 
  faFacebookSquare = faFacebookSquare; 

  brandEventFirstWord: string = '';
  brandEventText: string = '';

  contactsElement: ElementRef;

  @HostListener('window:scroll', ['$event'])
  scrollMenu(event) {
    if (window.pageYOffset > this.topDistance) {
      this.mastHeadTarget.style.top = '0px';
      this.mastHeadTarget.classList.add('u-no-transparent');

    } else {
      this.mastHeadTarget.classList.remove('u-no-transparent');
      this.mastHeadTarget.style.top = `${this.topDistance - window.pageYOffset}px`;
    }
  }

  // @ViewChild('topMenuBar', { static: false }) topMenuBarElement: ElementRef;

  @ViewChildren('pageContainer') pageContainer: QueryList<ElementRef>;
  @ViewChildren('topMenuBarRef') topMenuBarRef: QueryList<ElementRef>;
  @ViewChildren('mastHeadRef', { read: ElementRef }) mastHeadRef: QueryList<ElementRef>;

  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private globalUIGQLService: GlobalUIGQLService,
    private homedataService: HomedataService,
    public commondataService: CommondataService,
    public globalUIService: GlobalUIService,
  ) {

    this.commondataService.modalVisibilityUpdate.subscribe(
      ({ flag, title, content, fragment, type }) => {
        this.commondataService.modalVisible = flag;
        this.modalTitle = title;
        this.modalContent = content;
        this.modalFragment = fragment;
        this.modalType = type;
      }
    );

  }

  ngOnInit() {
    this.onFetchPage();

    this.globalUIGQLService.watch()
      .valueChanges
      .subscribe((result: any) => {
        console.log('globalUI', result);

        result.data.footer = { ...result.data.footer, ...result.data.contact }
        this.globalUIService.globalUI = result;

        this.commondataService.isSocial = result.data.allSocialProfiles.some((social: {}) => social['enable']);

        const eventName = this.globalUIService.globalUI.data.header.brandEventDescription.split(' ');
        this.brandEventFirstWord = eventName[0];
        this.brandEventText = eventName[1];

        if (this.commondataService.getCookie(this.commondataService.cookieName) === undefined) {
          this.commondataService.openModal(true, this.globalUIService.globalUI.data.footer.titoloModaleCookie, this.globalUIService.globalUI.data.footer.testoModaleCookie, 'choice');
        };

        if (!this.commondataService.markerPositions.length) {
          result.data.footer.contatti.map((contact: {}, index: number) => {
            if (contact['iconaContatto'] === 'Indirizzo') {
              this.commondataService.markerPositions.push({lat: contact['coordinate']['latitude'], lng: contact['coordinate']['longitude']});

              if (index === 0) this.commondataService.mapCenter = {lat: contact['coordinate']['latitude'] , lng: contact['coordinate']['longitude'] + 0.0004000};
            }
          })
        }

      });

    this.homedataService.watch()
      .valueChanges
      .subscribe(data => {
        console.log('myquery', data);
        this.header_content = data;
      });

    this.commondataService.sendElementRef.subscribe(
      (el: ElementRef) => this.contactsElement = el.nativeElement.offsetTop
    );
  }

  ngAfterViewInit() {
    const pageContainerReference = this.pageContainer.changes.subscribe(val => {
      let pageElement = this.pageContainer.toArray()[0];

      const menuBarReference = this.topMenuBarRef.changes.subscribe(val => {
        let mastHead: HTMLElement = Array.prototype.filter.call(pageElement.nativeElement.children, child => child.tagName === 'SPRK-MASTHEAD')[0];
        const mastHeadChild = mastHead.children[0] as HTMLElement;

        this.mastHeadTarget = mastHead;

        this.topDistance = this.topMenuBarRef.toArray()[0].nativeElement.offsetHeight;
        mastHead.style.top = this.topDistance + 'px';

        this.commondataService.mastheadTopDistance = mastHeadChild.offsetHeight;

        menuBarReference.unsubscribe();
        pageContainerReference.unsubscribe();

      });

      this.mastHeadRef.changes.subscribe(elements => this.commondataService.mastheadHeight = elements.first.nativeElement.firstChild.offsetHeight)
    });

  }


  onFetchPage() {
    this.fetchMenu();
    this.fetchPage();
  }


  private fetchPage() {
    this.http
      .get(
        'https://site-api.datocms.com/items', this.httpOptions
      )
      .subscribe(page => {
        console.log('pageContent', page);
        this.pageContent = page;
      });
  }

  private fetchMenu() {
    client.menuItems.all({
      filter: {
        ids: '147066, 201424, 278149, 222798, 147063'
      }
    })
    .then((menuItems) => {
      console.log('ITEMS', menuItems);
      console.log('SORT', menuItems.sort((a, b) => a.position - b.position));
      const sortedMenuItems = menuItems.sort((a, b) => a.position - b.position);
      this.menuList = sortedMenuItems.map(({ label }) => ({text: label, routerLink: label.replace(new RegExp(' ', 'g'), '_').toLowerCase()}));
    })
    .catch((error) => {
      console.error('NEW MENU ITEM ERROR', error);
    });

  }

  public getSantizeUrl(imgUrl : string) {
    return this.sanitizer.bypassSecurityTrustStyle(`url(${imgUrl})`);
  }

}
